import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}

import scala.io.Source.fromFile

object playDay {
  def main(args : Array[String]): Unit = {
    val conf = new SparkConf()
      .setAppName("GitHubPushCounter")
      .setMaster("local[*]")

    val sc = new SparkContext(conf)
    // println(sc)
    val sq = new SQLContext(sc)
    // println(sq)

    val homeDir = System.getenv("HOME")
    val inputPath = homeDir + "/temp/*.json"
    val gitLog = sq.read.json(inputPath)
    val pushes = gitLog.filter("type = 'PushEvent'")
    // println("input path: " + inputPath)

    // pushes.printSchema
    println("all events: " + gitLog.count)
    println("only pushes: " + pushes.count)
    pushes.show(5)

    val grouped = pushes.groupBy("actor.login").count().orderBy("count")
    val ordered = grouped.orderBy(grouped("count").desc)
    ordered.show(5)

    val employees = Set() ++ (
      for {
        line <- fromFile(homeDir + "/temp/ghEmployees.txt").getLines()
      } yield line.trim
    )
    val bcEmployees = sc.broadcast(employees)

    import sq.implicits._
    val isEmp = (user: String) => bcEmployees.value.contains(user)
    val isEmployee = sq.udf.register("SetContainsUdf", isEmp)
    val filtered = ordered.filter(isEmployee($"login"))
    filtered.show()
    }

}
