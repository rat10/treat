# treat


**treat** provides a library of basic aggregation scripts for analyzing Tor metrics data. These scripts should get the most elementary aggregations done and serve as examples and building blocks for writing any more specialized aggregations that you may need.  
**treat** is part of the [**trice**](https://gitlab.com/rat10/trice) analytics suite for Tor network metrics data.

The **trice** analytics suite supports three different analytics environments. No aggregation script will work on all of them equally well but there are considerable overlaps: all three environments support SQL which makes it the obvious common ground. There are differences though between the SQL dialect used in Spark and SnappyData ([truckSpark]() and [truckSnappy]()) and the one used in Greenplum ([truckGreen]()). All three environments support R in fairly the same way. Then there's also support for Python, Scala and Java in Spark and SnappyData.

If you are just starting with analytics and are not sure how you deep you will get into it, truckSpark (pure Spark) is a good choice. Every aggregation you use there you can reuse later in truckSnappy which is based on Spark and adds continuous intergation of data updates. If you already worked with PostgreSQL and have a regular need for analytics work, truckGreen is probably the way to go since it's a fork of PostgreSQL but heavily optimized for analytics workloads.



## TODO

### drill

don't forget about Drill which provides SQL over directories of files.  
that's very nice when e.g. appending the latest descriptors.

Drill provides only SQL and it can't process just any data you might want to throw at it but the constraints are well defined and practical means to overcome them are available.ing latest results



